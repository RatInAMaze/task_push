///
//  Generated code. Do not modify.
//  source: models.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Project$json = const {
  '1': 'Project',
  '2': const [
    const {'1': 'name', '3': 1, '4': 2, '5': 9, '10': 'name'},
  ],
};

const Task$json = const {
  '1': 'Task',
  '2': const [
    const {'1': 'text', '3': 1, '4': 2, '5': 9, '10': 'text'},
    const {'1': 'startTime', '3': 2, '4': 1, '5': 5, '10': 'startTime'},
    const {'1': 'endTime', '3': 3, '4': 1, '5': 5, '10': 'endTime'},
  ],
};

