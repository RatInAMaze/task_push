import 'package:flutter/material.dart';
import 'package:task_push/Screens/Home/home.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Task Pusher",
      home: Home(),
      theme: ThemeData(),
    );
  }
}
