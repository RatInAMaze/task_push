import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Task Pusher")),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: _buildTaskList(),
          ),
        ),
      ),
    );
  }

  Iterable<Widget> _buildTaskList() {
    List<Widget> widgets = new List<Widget>();

    // TODO: Do something smart to populate tasks
    for (int i = 0; i < 5; i++) {
      widgets.add(
          Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Center(
          child: Container(
            color: Color.fromARGB(255, 255, 0, 0),
            child: Text("Task $i"),
          ),
        )
      ]));
    }
    return widgets;
  }
}
